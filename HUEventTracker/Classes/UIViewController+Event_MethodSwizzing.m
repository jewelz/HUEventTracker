//
//  UIViewController+Event_MethodSwizzing.m
//  beautyAssistant
//
//  Created by huluobo on 2018/8/13.
//  Copyright © 2018 Service+. All rights reserved.
//

#import "UIViewController+Event_MethodSwizzing.h"
#import "NSObject+MethodSwizzing.h"
#import "UIResponder+HUEvent.h"
#import "HUEventTracker.h"
#import "HUEventObject.h"

@implementation UIViewController (Event_MethodSwizzing)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self exchangeOriginalSelector:@selector(viewDidLoad)
                  withSwizzingSelector:@selector(swizzing_viewDidLoad)];
        
        [self exchangeOriginalSelector:@selector(viewDidDisappear:)
                  withSwizzingSelector:@selector(swizzing_viewDidDisappear:)];
        
    });
}

- (void)swizzing_viewDidLoad {
    if (self.eventID && HUEventTracker.sharedInstance.enabled) {
        // 找到上个控制器
        UIViewController *lastVC = nil;
        if (self.navigationController) {
            if (self.navigationController.viewControllers.count > 1) {
                lastVC = self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
            }
        } else {
            lastVC = self.presentingViewController;
        }
        
        HUPageEventObject *event = [HUPageEventObject new];
        event.extraData = self.eventObj.extraData;
        event.eventType = @"page_init";
        event.pageID = self.eventID;
        event.lastPageID = lastVC.eventID;
        
        [[HUEventTracker sharedInstance] collectEvent:event];
    }
    
    [self swizzing_viewDidLoad];
}

- (void)swizzing_viewDidDisappear:(BOOL)animated {
    if (self.eventID && HUEventTracker.sharedInstance.enabled) {
        // 找到上个控制器
        UIViewController *lastVC = nil;
        if (self.navigationController) {
            if (self.navigationController.viewControllers.count > 1) {
                lastVC = self.navigationController.viewControllers[self.navigationController.viewControllers.count-2];
            }
        } else {
            lastVC = self.presentingViewController;
        }
        
        HUPageEventObject *event = [HUPageEventObject new];
        event.extraData = self.eventObj.extraData;
        event.eventType = @"page_exit";
        event.pageID = self.eventID;
        event.lastPageID = lastVC.eventID;
        
        [[HUEventTracker sharedInstance] collectEvent:event];
    }
    [self swizzing_viewDidDisappear:animated];
}

@end
