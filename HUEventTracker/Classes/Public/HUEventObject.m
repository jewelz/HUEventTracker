//
//  HUEvent.m
//  beautyAssistant
//
//  Created by huluobo on 2018/8/13.
//  Copyright © 2018 Service+. All rights reserved.
//

#import "HUEventObject.h"
#import "HUEventConfig.h"
#import "HUEventTracker.h"

@implementation HUEventObject

- (instancetype)init {
    if (self = [super init]) {
        NSTimeInterval timestamp = [[NSDate date] timeIntervalSince1970];
        _timestamp = timestamp;
        _eventID = [NSString stringWithFormat:@"%@-%f", [NSUUID UUID].UUIDString, timestamp*100000];
    }
    return self;
}

@end

@implementation HUEventObject (JSON)

- (NSDictionary *)jsonData {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (!_config) {
        _config = [HUEventTracker sharedInstance].currentConfig;
    }
    
    if (_config.businessLine) {
        dict[@"business_line"] = _config.businessLine;
    }
    if (_config.businessVersion) {
        dict[@"business_version"] = _config.businessVersion;
    }
    if (_config.version) {
        dict[@"version"] = _config.version;
    }
    if (_config.platform) {
        dict[@"viewer_platform"] = _config.platform;
    }
    if (_pageID) {
        dict[@"current_page"] = _pageID;
    }
    if (_objectID) {
        dict[@"button_index"] = _objectID;
    }
    if (_userID) {
        dict[@"page_viewer"] = _userID;
    }
    if (!_clinet) {
        _clinet = @"side_of_b";
    }
    dict[@"viewer_side"] = _clinet;
    if (_extraData) {
        dict[@"extension_data"] = _extraData;
    }

    if (!_customData) {
        _customData = [HUEventTracker sharedInstance].currentConfig.customData;
    }
    if (_customData) {
        [_customData enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            dict[key] = obj;
        }];
    }
    
    dict[@"action_timestamp"] = @(_timestamp);
    dict[@"event"] = _eventType;

    return dict.copy;
}

- (BOOL)isEqual:(id)object {
    if (![object isKindOfClass:HUEventObject.class]) {
        return false;
    }
    return [_eventID isEqualToString:[object eventID]];
}


@end

@implementation HUPageEventObject

- (NSDictionary *)jsonData {
    NSMutableDictionary *dict = [[super jsonData] mutableCopy];
    if (_lastPageID) {
        dict[@"from_page"] = _lastPageID;
    }
    return dict.copy;
}

@end
