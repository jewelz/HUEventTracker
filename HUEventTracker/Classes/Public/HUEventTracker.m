//
//  HUEventTracker.m
//  beautyAssistant
//
//  Created by huluobo on 2018/8/13.
//  Copyright © 2018 Service+. All rights reserved.
//

#import "HUEventTracker.h"
#import "HUEventCollector.h"
#import "HUEventUploader.h"
#import <AFNetworking/AFNetworking.h>

@interface HUEventTracker() {
   
}

@property (nonatomic, strong) HUEventConfig *currentConfig;

@end

@implementation HUEventTracker

+ (nonnull instancetype)sharedInstance {
    static HUEventTracker *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;

}

- (instancetype)init {
    return [super init];
}

+ (void)startWithConfig:(nonnull HUEventConfig *)config {
    [[self sharedInstance] startWithConfig:config];
}

+ (void)registerPageWithPlist:(NSString *)plistFile {
    [[self sharedInstance] registerPageWithPlist:plistFile];
}

- (void)registerPageWithPlist:(NSString *)plistFile {
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:plistFile];
    if (dict == nil) {
        plistFile =  [[NSBundle mainBundle] pathForResource:plistFile ofType:nil];
    }
    
    if (plistFile == nil) {
        plistFile =  [[NSBundle mainBundle] pathForResource:@"Info.plist" ofType:nil];
    }
    dict = [NSDictionary dictionaryWithContentsOfFile:plistFile];
    NSAssert(dict[@"HUEventTrackerPages"], @"Can not get value for key `pages`.");
    
    _pageMap = dict[@"HUEventTrackerPages"];
}

- (void)collectEvent:(HUEventObject *)event {
    if (!_enabled) { return; }
    [[HUEventCollector sharedInstance] collectEvent:event];
    
    // 目前只要收集了事件就发送，先这样做着，以后再改
    [self sendEvent:event];
}

- (void)sendAllEvents {
    for (HUEventObject *event in [[HUEventCollector sharedInstance] allEvents]) {
        [self sendEvent:event];
    }
}

- (void)sendEvent:(HUEventObject *)event {
    if (!_enabled) { return; }
    [[HUEventUploader sharedInstance] uploadEvent:event];
}

- (void)startWithConfig:(nonnull HUEventConfig *)config {
    _currentConfig = config;
}



- (void)tmpUpload {
    
}

@end
