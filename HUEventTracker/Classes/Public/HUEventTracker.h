//
//  HUEventTracker.h
//  beautyAssistant
//
//  Created by huluobo on 2018/8/13.
//  Copyright © 2018 Service+. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HUEventConfig.h"
#import "HUEventObject.h"
#import "UIResponder+HUEvent.h"

@interface HUEventTracker : NSObject {
    @protected
     NSDictionary *_pageMap;
}


+ (nonnull instancetype)sharedInstance;

+ (void)startWithConfig:(nonnull HUEventConfig *)config;

+ (void)registerPageWithPlist:(nullable NSString *)plist;

@property (nonatomic, strong, readonly) HUEventConfig *currentConfig;
@property (nonatomic) BOOL enabled;

/**
 * 发送所有事件，会立即将事件发送给服务器。
 */
- (void)sendAllEvents;

/**
 * 发送事件，会立即将事件发送给服务器。
 */
- (void)sendEvent:(nonnull HUEventObject *)event;

/**
 *  收集事件
 */
- (void)collectEvent:(nonnull HUEventObject *)event;

- (instancetype)init UNAVAILABLE_ATTRIBUTE;
+ (instancetype)new UNAVAILABLE_ATTRIBUTE;
@end
