//
//  HUEventCollector.m
//  beautyAssistant
//
//  Created by huluobo on 2018/8/13.
//  Copyright © 2018 Service+. All rights reserved.
//

#import "HUEventCollector.h"
#import "HUEventTracker.h"
@interface HUEventCollector() {
    NSMutableArray<HUEventObject *> *_events;
    dispatch_queue_t _ioQueue;
}

@end

@implementation HUEventCollector

+ (nonnull instancetype)sharedInstance {
    static HUEventCollector *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self new];
    });
    return instance;
}

- (instancetype)init {
    if (self = [super init]) {
        _events = [NSMutableArray array];
        _ioQueue = dispatch_queue_create("com.jewelz.HUEventCollector.concurrent", DISPATCH_QUEUE_CONCURRENT);
    }
    return self;
}

- (void)collectEvent:(HUEventObject *)event {
    if (event.eventID == nil) {
        return;
    }
    
    dispatch_barrier_async(_ioQueue, ^{
        [self->_events addObject:event];
    });
}

- (void)removeEvent:(HUEventObject *)event {
    if (event.eventID == nil) {
        return;
    }

    dispatch_barrier_async(_ioQueue, ^{
        [self->_events removeObject:event];
    });
}

- (NSArray<HUEventObject *> *)allEvents {
    __block NSArray *events = nil;
  
    dispatch_sync(_ioQueue, ^{
        events = [self->_events copy];
    });
    return events;
}

@end
