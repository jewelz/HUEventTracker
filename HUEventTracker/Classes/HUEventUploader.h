//
//  HUEventUploader.h
//  beautyAssistant
//
//  Created by huluobo on 2018/8/13.
//  Copyright © 2018 Service+. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HUEventObject;
@interface HUEventUploader : NSObject

+ (nonnull instancetype)sharedInstance;

- (void)uploadEvent:(nonnull HUEventObject *)event;

@end
