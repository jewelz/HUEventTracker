//
//  HUEventTracker+Extension.h
//  HUEventTracker
//
//  Created by huluobo on 2018/8/17.
//

#import <HUEventTracker/HUEventTracker.h>

@interface HUEventTracker (Extension)


- (nullable)pageIDForPage:(nonnull NSString *)page;

@end
