//
//  UIApplication+MethodSwizzing.m
//  beautyAssistant
//
//  Created by huluobo on 2018/8/10.
//  Copyright © 2018 Service+. All rights reserved.
//

#import "UIApplication+Event_MethodSwizzing.h"
#import "NSObject+MethodSwizzing.h"
#import "HUEventTracker.h"
#import "HUEventObject.h"
#import "UIResponder+HUEvent.h"


@implementation UIApplication (Event_MethodSwizzing)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self exchangeOriginalSelector:@selector(sendAction:to:from:forEvent:) withSwizzingSelector:@selector(swizzing_sendAction:to:from:forEvent:)];
    });
}

- (BOOL)swizzing_sendAction:(SEL)action to:(nullable id)target from:(nullable id)sender forEvent:(nullable UIEvent *)event {
    if ([sender isKindOfClass:UIResponder.class] && [sender eventID] && HUEventTracker.sharedInstance.enabled) {

        // 先通过 target 获取控制器，如果没找到则通过 sender 获取
        UIViewController *currentVC = findTheViewController(target);
        if (!currentVC) {
            currentVC = findTheViewController(sender);
        }
        
        // 找到了当前控制器后，找到上个控制器
        UIViewController *lastVC = nil;
        if (currentVC.navigationController) {
            if (currentVC.navigationController.viewControllers.count > 1) {
                lastVC = currentVC.navigationController.viewControllers[currentVC.navigationController.viewControllers.count-2];
            }
        } else {
            lastVC = currentVC.presentingViewController;
        }

        
        HUEventObject *eventObjec = [sender eventObj];
        if (!eventObjec) {
            eventObjec = [HUEventObject new];
        }
        eventObjec.objectID = [sender eventID];
        eventObjec.eventType = @"btn_click";
        eventObjec.pageID = currentVC.eventID;
        [[HUEventTracker sharedInstance] collectEvent:eventObjec];
    }
    
    return [self swizzing_sendAction:action to:target from:sender forEvent:event];
}

@end

