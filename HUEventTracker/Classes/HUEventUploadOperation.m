//
//  - (instancetype)initWithEvent:(HUEventObject *)event; HUEventUploadOperation.m
//  beautyAssistant
//
//  Created by huluobo on 2018/8/13.
//  Copyright © 2018 Service+. All rights reserved.
//

#import "HUEventUploadOperation.h"
#import "HUEventObject.h"
#import <AFNetworking/AFNetworking.h>

@interface HUEventUploadOperation(){
    HUEventObject *_event;
    BOOL        executing;
    BOOL        finished;
}

@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;
@property (nonatomic, strong) UploadEventCompletion completion;

@end

@implementation HUEventUploadOperation

- (instancetype)initWithEvent:(HUEventObject *)event uploadCompletion:(UploadEventCompletion)completion {
    if (self = [super init]) {
        _event = event;
        _completion = completion;
        executing = NO;
        finished = NO;
    }
    return self;
}

- (void)start {
    if (self.isCancelled) {
        [self willChangeValueForKey:@"isFinished"];
        finished = YES;
        [self didChangeValueForKey:@"isFinished"];
        return;
    }
    
    [self willChangeValueForKey:@"isExecuting"];
    [self beginTask];
    executing = YES;
    [self didChangeValueForKey:@"isExecuting"];
}

- (void)beginTask {
#if DEBUG
    NSLog(@"============== Begin upload ID: %@ =============", _event.eventID);
    NSLog(@"uploding data: \n%@", _event.jsonData);
#endif
    
    NSString *urlStr = @"https://flume.meiyezhushou.com";
#if DEBUG
    urlStr = @"http://devflume.meiyezhushou.com";
#endif
    
    NSData *bodyData = [NSJSONSerialization dataWithJSONObject:_event.jsonData options:NSJSONWritingPrettyPrinted error:nil];
    NSString *bodyJSONString = [[NSString alloc] initWithData:bodyData encoding:NSUTF8StringEncoding];
    
    NSArray *params = @[
                        @{
                            @"headers": @{ @"k": @"v"},
                            @"body": bodyJSONString
                            }
                        ];
#if DEBUG
    NSLog(@"Send request URL: %@, \t\nBody: %@", urlStr, params);
#endif
    
    __weak __typeof(self) wself = self;
    [self.sessionManager POST:urlStr
                   parameters:params
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          __strong __typeof(self) sself = wself;
#if DEBUG
                          NSLog(@"Upload Success");
#endif
                          sself.completion(sself->_event, nil);
                          [sself.sessionManager.session finishTasksAndInvalidate];
                          [self willChangeValueForKey:@"isFinished"];
                          sself->finished = YES;
                          [self didChangeValueForKey:@"isFinished"];
                          
                      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          __strong __typeof(self) sself = wself;
          
                          sself.completion(sself->_event, error);
                          [sself.sessionManager.session finishTasksAndInvalidate];
                          [self willChangeValueForKey:@"isFinished"];
                          sself->finished = YES;
                          [self didChangeValueForKey:@"isFinished"];
                      }];
}

- (BOOL)isConcurrent {
    return YES;
}

- (BOOL)isExecuting {
    return executing;
}

- (BOOL)isFinished {
    return finished;
}

- (AFHTTPSessionManager *)sessionManager {
    if (_sessionManager == nil) {
        _sessionManager = [[AFHTTPSessionManager alloc] init];
        _sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
        _sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        _sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/plain", @"application/json", @"text/json", @"text/javascript", @"text/html",nil];
    }
    return _sessionManager;
}

@end
