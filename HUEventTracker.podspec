#
# Be sure to run `pod lib lint HUEventTracker.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'HUEventTracker'
  s.version          = '0.1.7'
  s.summary          = '数据埋点.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  数据埋点服务.
                       DESC

  s.homepage         = 'https://gitlab.com/jewelz/HUEventTracker'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'huluobo' => 'hujewelz@163.com' }
  s.source           = { :git => 'https://gitlab.com/jewelz/HUEventTracker.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'HUEventTracker/Classes/**/*'
  s.public_header_files = 'HUEventTracker/Classes/Public/*.h'
  s.dependency 'AFNetworking'

end
